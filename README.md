# helm-example
```https://helm.sh/
https://helm.sh/docs/intro/quickstart/
```

# list
```bash
helm <cmd> --help
helm ls
```

# create and pack
```bash
helm create src01
helm package src01
```

# structure
```bash
Chart.yaml
values.yaml
templates
charts
```

# install upgrade
## install
```bash
helm install ${APP_NAME} ${SRC_DIR}
helm install app01 src01
helm install app01 src01 --namespace dev
helm install app01 src01 -n dev
```

## upgrade
```bash
helm upgrade app01 src01 -n=dev
helm upgrade app01 src01 --set appVersion=1.17.8
```

## delete
```bash
helm ls -n=dev
helm delete app1 -n dev
```

# Other
## dry-run & debug
```bash
helm upgrade --install app01 src01 -n dev
helm upgrade --install app01 src01 -n dev --dry-run --debug

helm upgrade --install app01 src01 --set replicaCount=2 -n dev
helm upgrade --install app01 src01 --set replicaCount=2 -n dev --dry-run
```

# Add Repo
```bash
helm repo add ${REPO_NAME} ${REPO_URL}
helm repo add grafana https://grafana.github.io/helm-charts

helm repo update
helm search repo grafana
#grafana/grafana
helm upgrade --install grafana grafana/grafana
```

